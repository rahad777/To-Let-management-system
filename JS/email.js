$(document).ready(function() {

    // Generate a simple captcha
    $('#emailform').bootstrapValidator({

        message: 'This value is not valid',

        fields: {
            username: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The username is required and can\'t be empty'
                    },
                    stringLength: {

                        min: 2,

                        max: 30,

                        message: 'The username must be more than 2 and less than 30 characters long'

                    }
                }
            },
            user_email: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The username is required and can\'t be empty'
                    },
                    emailAddress: {

                        message: 'The input is not a valid email address'

                    }
                }
            }
        }
    });
});
$(document).ready(function(){
    setTimeout(function(){ $('#message').fadeOut() }, 5000);
});
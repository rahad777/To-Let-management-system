$(document).ready(function() {

    // Generate a simple captcha
    $('#birthform').bootstrapValidator({

        message: 'This value is not valid',

        fields: {
            username: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The username is required and can\'t be empty'
                    },
                    regexp: {

                        regexp: /^[a-zA-Z_\.]+$/,

                        message: 'The username can only consist of alphabetical, number, dot and underscore'

                    },
                    stringLength: {

                        min:2,

                        max: 30,

                        message: 'The username must be more than 2 and less than 30 characters long'

                    }
                }
            },
            birthdate: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The username is required and can\'t be empty'
                    }
                }
            }
        }
    });
});

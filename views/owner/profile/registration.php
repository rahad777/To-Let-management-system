<?php
require_once("../../../vendor/autoload.php");
use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;
if (isset($_FILES['image'])) {
    $errors=array();
    $name = time() . $_FILES['image']['name'];
    $file_size=$_FILES['image']['size'];
    $tmp = $_FILES['image']['tmp_name'];
    $file_type=$_FILES['image']['type'];
    $file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));
    $format=array("jpeg","jpg","png");
    if (in_array($file_ext,$format)==false){
        $errors[]="extension not allowed,please choose a jpeg or png file";
    }
    if ($file_size>2097152){
        $errors[]="file size must be less then 2 mb";
    }
    move_uploaded_file($tmp, 'upload/' . $name);


    $_POST['image'] = $name;
}
$auth= new Auth();
$auth->setData($_POST);// this setData() is equivalent to setData()
$status=     $auth->is_exist();
if($status){
    Message::setMessage("<div class='alert alert-danger'>
    <strong>Taken!</strong> Email has already been taken. </div>");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}else{
    $_POST['email_token'] = md5(uniqid(rand()));

    $obj= new User();
    $obj->setData($_POST); // this setData() is equivalent to setData()
    $obj->store();
    require '../../../../vendor/phpmailer/phpmailer/PHPMailerAutoload.php';
    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->SMTPDebug  = 0;
    $mail->SMTPAuth   = true;
    $mail->SMTPSecure = "ssl";
    $mail->Host       = "smtp.gmail.com";
    $mail->Port       = 465;
    $mail->AddAddress($_POST['email']);
    $mail->Username="XXXXXXXXXX@gmail.com";               //       your gmail address
    $mail->Password="XXXXXXXXXXXX";                        //       your gmail password
    $mail->SetFrom('bitm.php22@gmail.com','User Management');
    $mail->AddReplyTo("bitm.php22@gmail.com","User Management");
    $mail->Subject    = "Your Account Activation Link";
    $message =  "Please click this link to verify your account: 
       http://localhost/UserManagement/views/SEIPXXXX/User/Profile/emailverification.php?email=".$_POST['email']."&email_token=".$_POST['email_token'];
    $mail->MsgHTML($message);
    $mail->Send();
}
<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../bootstrap/css/bootstrap.min.css">
    <script src="../../../bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../bootstrap/js/jquery.min.js"></script>
</head>
<div class="container">
    <div class="jumbotron">
    </div>
    <div class="row">
<div class="col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-md-6 col-lg-6 col-sm-6" >
    <div class="panel panel-default">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="nav navbar-middle">
                     <h2>REGISTRATION</h2>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="well" id="navbar">
                <form action="registration.php" id="form" method="post" role="form" enctype="multipart/form-data">
                    <div class="form-group"  >
                        <label for="ownername">Full Name</label>
                        <input type="text" name="ownername" id="name" class="form-control" placeholder="please enter your full name">
                    </div>
                    <div class="form-group">
                        <label for="owneremail">Email</label>
                        <input type="text" name="owneremail" id="email" class="form-control" placeholder="please enter your email">
                    </div>
                    <div class="form-group">
                        <label for="ownerphone">Phone No</label>
                        <input type="text" name="ownerphone" id="phone" class="form-control" placeholder="please enter your phone no" >
                    </div>
                    <div class="form-group">
                        <label for="username">User NAME</label>
                        <input type="text" name="username" id="user" class="form-control" placeholder="please enter a username">
                    </div>
                    <div class="form-group">
                        <label for="username">Password</label>
                        <input type="password" name="username" id="user" class="form-control" placeholder="please enter a password">
                    </div>
                    <div class="form-group">
                        <label for="username">Confirm Password</label>
                        <input type="password" name="username" id="user" class="form-control" placeholder="please re-enter password">
                    </div>
                    <div class="form-group">
                        <label for="ownergender">Gender</label>
                        <label class="radio-inline">
                            <input type="radio" name="gender" value="Male">Male</label>
                        <label class="radio-inline">
                            <input type="radio" name="gender" value="Female">Female</label>
                    </div>
                    <div class="form-group">
                        <label>Select image to Upload:</label>
                        <input type ="file" name="image" id="filetoupload">
                    </div>
                    <button type="submit" class="btn btn-info">Submit</button>
                </form>
            </div>
            </div>

